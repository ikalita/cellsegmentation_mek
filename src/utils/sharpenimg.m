function [simg] = sharpenimg(img,n)

nstacks = 6;
z0  = (nstacks+1)/2;
dim = 10;
x0 = dim+1;
y0 = dim+1;
filt = zeros(2*dim+1,2*dim+1,nstacks);
for xp = 1:size(filt,1)
    x = xp-x0;
    for yp = 1:size(filt,2)
        y = yp-y0;
        for zp = 1:nstacks
            z = zp-z0;
            d = sqrt(x^2+y^2+z^2);
            filt(xp,yp,zp) = 1./(d^n);
        end
    end
end
filt = filt./(max(filt(:)));
filt2 = zeros(2*dim+1,2*dim+1);
filt2(:) = sum(reshape(filt,(2*dim+1)*(2*dim+1),nstacks)');

filt2 = fspecial('gaussian',5,1);

simg  = deconvlucy(img,filt2);

end