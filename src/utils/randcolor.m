function color = randcolor();

r = rand();
if(r<0.33)
    color = ([rand(),rand(),0]+0.2)./1.2;
elseif(r<0.66)
    color = ([rand(),0,rand()]+0.2)./1.2;
else
    color = ([0,rand(),rand()]+0.2)./1.2;
    %colors = (rand(ucells,3)+0.2)./1.2;
end
end