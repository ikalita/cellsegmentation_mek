function [img] = rotateimg(img,angle)

img = imrotate(img,angle,'bilinear','crop');

%TODO

% $$$ 
% $$$ tempgrid = zeros(7,7);
% $$$ 
% $$$ x = cos(pi*angle/180)*1;
% $$$ y = sin(pi*angle/180)*1;
% $$$ filt = normdiscmat(tempgrid,[x,y],sig);
% $$$ 
% $$$ img = imfilter(img,filt,'replicate');%,'conv'

end