function [img,time] = loadmatimg(frame)

    temp = load(frame.filepath);
    img  = double(temp.img);
    if(isfield(temp,'deltat'))
        time = temp.deltat;
    end
    
end