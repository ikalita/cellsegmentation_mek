function [frameset] = makeframeset(folder,nits)
% Added option to set the maximum iteration
    imgfiles = dir([folder,'IMG-*.mat']);
    if(length(imgfiles)>0)
        [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);
        if(length(steps)==0)
            nsteps = 1;
            steps = {''};
        else
            nsteps = length(steps);
        end
        if(nits>0)
        else
            nits = length(its);
        end
        frameset = cell(nsteps,length(xys),length(chns),nits);
        for s = 1:length(steps)
            for xy = 1:length(xys)
                for ch = 1:length(chns)
                    for it = 1:nits
                        if(~strcmp(steps{s},''))
                            filename = ['IMG-',prefix,...
                                        '_ST-',steps{s},...
                                        '_XY-',xys{xy},...
                                        '_CHN-',chns{ch},...
                                        '_IT-',its{it},...
                                        '.mat'];
                        else
                            filename = ['IMG-',prefix,...
                                        '_XY-',xys{xy},...
                                        '_CHN-',chns{ch},...
                                        '_IT-',its{it},...
                                        '.mat'];
                        end
                        filepath = [folder,filename];
                        if(~exist(filepath))
                            error(['File does not exists: ',filepath]);
                        end
                        frame = struct;
                        frame.prefix    = prefix;
                        frame.step      = steps{s};
                        frame.xy        = xys{xy};
                        frame.chn       = chns{ch};
                        frame.it        = its{it};
                        frame.folder    = folder;
                        frame.filename  = filename;
                        frame.filepath  = filepath;
                        frameset{s,xy,ch,it} = frame;
                    end
                end
            end            
        end
    end

end