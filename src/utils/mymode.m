function [mod] = mymode(vals,ns,minval)

pos  = find(vals>minval);
vals = vals(pos);
svals  = smooth(sort(vals,'ascend'),ns);
cumfun = cumsum(ones(size(svals)))./length(svals);

% $$$ deltavals = diff(svals);
% $$$ deltacumf = diff(1./cumfun);
% $$$ 
% $$$ der = diff(cumfun./svals);

[~,maxInd] = max(cumfun./svals); 

% $$$ figure();
% $$$ subplot(2,1,1);
% $$$ plot(svals,cumfun);
% $$$ set(gca,'xscale','log');
% $$$ subplot(2,1,2);
% $$$ plot(svals,cumfun./svals);
% $$$ set(gca,'xscale','log');

mod = svals(maxInd);

end