function [prefix,steps,xys,chns,its] = parseDLfiles(imgfiles)
    Sprefixes  = struct;
    Ssteps     = struct;
    Sxys       = struct;
    Schns      = struct;
    Sits       = struct;
    for n = 1:length(imgfiles)
        name = imgfiles(n).name;
        if(strfind(name,'thumb'))
            continue;
        end
        % Let's assume that the prefix is XX_XX_
        unders = strfind(name,'_');
        prefix = name(1:unders(2)-1);
        rest   = name((unders(2)+1):end);
        xyp    = strfind(rest,'_');
        xy     = rest(1:xyp-1);
        rest   = rest(xyp+1:end);
        chp    = strfind(rest,'LED')-2;
        endp   = strfind(rest,'.TIF');
        chn    = rest(chp:endp-1);
        it     = '';
% $$$ % $$$         chp  = strfind(name,'LED')-2;
% $$$ % $$$         endp = strfind(name,'.TIF');
% $$$ % $$$         if(strfind(name,'_t'))
% $$$ % $$$             itp  = strfind(name,'_t');
% $$$ % $$$             prefix  = name(1:chp-2);
% $$$ % $$$             it      = name(itp+1:endp-1);
% $$$ % $$$             chn     = name(chp:itp-1);
% $$$         elseif(strfind(name,'2h_'))
% $$$             itp     = strfind(name,'2h_');
% $$$             prefix  = name(1:itp+1);
% $$$             it      = name(itp+3:chp-2);
% $$$             chn     = name(chp:endp-1);
% $$$         elseif(strfind(name,'_2h'))
% $$$             itp     = strfind(name,'_2h');
% $$$             prefix  = name(1:itp-1);
% $$$             it      = name(itp+1:chp-2);
% $$$             chn     = name(chp:endp-1);
% $$$         else
% $$$             error('Unknown naming convention');
% $$$         end
        chn     = strrep(chn,'-','asdf42');
        chn     = strrep(chn,' ','asdf23');
        %xyp = strfind(name,'_XY-');
        %xy  = strrep(name(xyp+4:chp-1),'-','_');
        Sxys.(['T',xy])          = 1;
        Sprefixes.(['T',prefix]) = 1;
        Schns.(['T',chn])        = 1;
        Sits.(['T',it])          = 1;
    end
    prefixes  = fieldnames(Sprefixes);
    steps     = fieldnames(Ssteps);
    xys       = sort(fieldnames(Sxys));
    chns      = sort(fieldnames(Schns));
    its       = fieldnames(Sits);
    if(length(prefixes)>1)
        display('Ooops!');
    else
        prefix = prefixes{1}(2:end);
    end
    for n = 1:length(steps)
        steps{n} = steps{n}(2:end);
    end
    for n = 1:length(xys)
        xys{n} = strrep(xys{n}(2:end),'_','-');
    end
    for n = 1:length(chns)
        chns{n} = chns{n}(2:end);
        chns{n} = strrep(chns{n},'asdf42','-');
        chns{n} = strrep(chns{n},'asdf23',' ');
    end
    for n = 1:length(its)
        its{n} = its{n}(2:end);
    end
end
