function [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles)
    Sprefixes  = struct;
    Ssteps     = struct;
    Sxys       = struct;
    Schns      = struct;
    Sits       = struct;
    hyphenON   = 0;
    for n = 1:length(imgfiles)
        name = imgfiles(n).name;
        if(isempty(strfind(name,'_ST-')))
            % Other
            xyp = strfind(name,'_XY-');
            chp = strfind(name,'_CHN-');
            itp = strfind(name,'_IT-');
            prefix  = name(5:xyp-1);
            prefix  = strrep(prefix,'-','asdf66');
            xy      = name(xyp+4:chp-1);
            if(regmbool(xy,'-'))
                hyphenON = 1;
                xy       = strrep(xy,'-','_');
            end
            chn     = name(chp+5:itp-1);
            it      = name(itp+4:end-4);
            Sprefixes.(['T',prefix]) = 1;
            Sxys.(['T',xy])          = 1;
            Schns.(['T',chn])        = 1;
            Sits.(['T',it])          = 1;
        else
            % MACS
            stp = strfind(name,'_ST-');
            xyp = strfind(name,'_XY-');
            chp = strfind(name,'_CHN-');
            itp = strfind(name,'_IT-');
            prefix  = name(5:stp-1);
            step    = name(stp+4:xyp-1);
            xy      = name(xyp+4:chp-1);
            chn     = name(chp+5:itp-1);
            it      = name(itp+4:end-4);
            Sprefixes.(['T',prefix]) = 1;
            Ssteps.(['T',step])      = 1;
            Sxys.(['T',xy])          = 1;
            Schns.(['T',chn])        = 1;
            Sits.(['T',it])          = 1;
        end
    end
    prefixes  = fieldnames(Sprefixes);
    steps     = fieldnames(Ssteps);
    xys       = fieldnames(Sxys);
    chns      = fieldnames(Schns);
    its       = fieldnames(Sits);
    if(length(prefixes)>1)
        display('Ooops!');
    else
        prefix = strrep(prefixes{1}(2:end),'asdf66','-');
        %prefix = prefixes{1}(2:end);
    end
    for n = 1:length(steps)
        steps{n} = steps{n}(2:end);
    end
    for n = 1:length(xys)
        if(hyphenON)
            xys{n} = strrep(xys{n}(2:end),'_','-');
        else
            xys{n} = xys{n}(2:end);
        end
    end
    for n = 1:length(chns)
        chns{n} = chns{n}(2:end);
    end
    for n = 1:length(its)
        its{n} = its{n}(2:end);
    end
end
