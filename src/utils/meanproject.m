function [mproj] = meanproject(stack)

[nx,ny,ns] = size(stack);
mproj = zeros(nx,ny);
mproj(:) = mean(reshape(stack(:,:,:),nx*ny,ns)');

end