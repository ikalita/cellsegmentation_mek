function [img] = loadTIF(filepath)

    img = double(imread(filepath));
    
end