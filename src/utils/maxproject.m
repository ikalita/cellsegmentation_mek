function [mproj] = maxproject(stack)

[nx,ny,ns] = size(stack);
mproj = zeros(nx,ny);
mproj(:) = max(reshape(stack(:,:,:),nx*ny,ns)');

end