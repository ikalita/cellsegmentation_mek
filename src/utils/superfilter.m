function [f] = superfilter(dim,gam,n)

% $$$ filter=-1*ones(dim,dim); 
% $$$ filter((dim+1)/2,(dim+1)/2)=dim^2-1; %lf1xlf1 filter
dim;
f = ones(size(dim,dim));
dim0 = dim/2;
for i = 1:dim
    for j = 1:dim
        d = sqrt((i-0.5-dim0)^2+(j-0.5-dim0)^2);
        f(i,j) = 1/(1+(d/gam)^2);
    end
end
f=f;
%f=f/sum(sum(f)); 
f=f-max(max(f));
%f=-f;
%f=zeros(dim,dim);
%f=-1*ones(dim,dim); 
f(dim0+0.5,dim0+0.5) = dim^n-1;
% $$$ f(1,:) = -f(1,:);
% $$$ f(end,:) = -f(end,:);
% $$$ f(:,1) = -f(:,1);
% $$$ f(:,end) = -f(:,end);

end
