function [date] = getdate()
    t = clock;
    tyear = num2str(t(1));
    if(t(2)<10)
        tmonth = ['0',num2str(t(2))];
    else
        tmonth = num2str(t(2));
    end
    if(t(3)<10)
        tday = ['0',num2str(t(3))];
    else
        tday = num2str(t(3));
    end
    if(t(4)<10)
        thour = ['0',num2str(t(4))];
    else
        thour = num2str(t(4));
    end
    if(t(5)<10)
        tmin = ['0',num2str(t(5))];
    else
        tmin = num2str(t(5));
    end
    if(t(6)<10)
        tsec = ['0',num2str(t(6))];
    else
        tsec = num2str(t(6));
    end
    date = [tyear,tmonth,tday,'_',thour,'h',tmin,'m',tsec,'s'];
end