function [mf,mfsum] = normdiscmat(m0,mu,sig)

mf = zeros(size(m0));
[nx,ny] = size(m0);

sig2 = [sig,0;0,sig];

x0 = nx/2;
y0 = ny/2;

tx = mu(1);
ty = mu(2);

xs = [0:nx-1];
xs = xs-x0+0.5;
ys = [0:ny-1];
ys = ys-y0+0.5;

for i = 1:nx
    x1 = xs(i)-0.5;
    x2 = xs(i)+0.5;
    for j = 1:ny
        y1 = ys(j)-0.5;
        y2 = ys(j)+0.5;
        mf(i,j) = (mvncdf([x1,y1],mu,sig2)+mvncdf([x2,y2],mu,sig2))-(mvncdf([x1,y2],mu,sig2)+mvncdf([x2,y1],mu,sig2));
    end
end

mfsum = sum(sum(mf));

mf = mf./mfsum;

end