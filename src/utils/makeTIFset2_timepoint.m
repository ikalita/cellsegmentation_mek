function [] = makeTIFset2(foldername,prefix,channels,indexes,timepoints)

% if isempty(timepoints)
    %frameset = cell(1,length(indexes),length(channels),1);
    frameset = cell(1,length(indexes),length(channels),length(timepoints));
    for it = 1:timepoints
        for xy = 1:length(indexes)
            index = num2str(indexes(xy));
            timepoint = num2str(it);
            for ch = 1:length(channels)
                channel = channels{ch};
                filename = [prefix,'_',channel,'_s',index,'_t',timepoint,'.TIF'];
                %filename = [prefix,'_',channel,'_s',index,'.TIF'];
                filepath = [foldername,filename];
                if(~exist(filepath))
                    error(['File does not exists: ',filepath]);
                end
                frame = struct;
                frame.prefix    = prefix;
                frame.step      = '';
                frame.xy        = index;
                frame.chn       = channel;
                frame.it        = it;
                frame.folder    = foldername;
                frame.filename  = filename;
                frame.filepath  = filepath;
                frameset{1,xy,ch,it} = frame;
%                 frameset{1,xy,ch,1} = frame;
            end
        end
    end
    save(['./',prefix,'_time.mat'],'frameset');
% else
%     error('Need to be implemented');
% end


end