function [] = makemovies(folder,maximg,minimg,fps)


    imgfiles = dir([folder,'IMG-*.mat']);
    if(length(imgfiles)>0)
        [prefix,steps,xys,chns,its] = parseIMGfiles(imgfiles);
        if(length(steps)==0)
            nsteps = 1;
            steps = {''};
        else
            nsteps = length(steps);
        end
        frameset = cell(nsteps,length(xys),length(chns),length(its));
        for s = 1:length(steps)
            for xy = 1:length(xys)
                for ch = 1:length(chns)
                    for it = 1:length(its)
                        if(~strcmp(steps{s},''))
                            filename = ['IMG-',prefix,...
                                        '_ST-',steps{s},...
                                        '_XY-',xys{xy},...
                                        '_CHN-',chns{ch},...
                                        '_IT-',its{it},...
                                        '.mat'];
                        else
                            filename = ['IMG-',prefix,...
                                        '_XY-',xys{xy},...
                                        '_CHN-',chns{ch},...
                                        '_IT-',its{it},...
                                        '.mat'];
                        end
                        filepath = [folder,filename];
                        if(~exist(filepath))
                            error(['File does not exists: ',filepath]);
                        end
                        frame = struct;
                        frame.prefix    = prefix;
                        frame.step      = steps{s};
                        frame.xy        = xys{xy};
                        frame.chn       = chns{ch};
                        frame.it        = its{it};
                        frame.folder    = folder;
                        frame.filename  = filename;
                        frame.filepath  = filepath;
                        frameset{s,xy,ch,it} = frame;
                    end
                end
            end            
        end
        for s = 1:length(steps)
            for xy = 1:length(xys)
                fig  = figure('Visible','off','MenuBar','none','Position',[0,0,length(chns)*270,280]);
                for ch = 1:length(chns)
                    ha(ch) = axes('Units','Pixels','Position',[(ch-1)*(256+10)+10,3,512/2,512/2]);
                end
                vidObj = VideoWriter([prefix,'_XY',xys{xy},'.avi']);
                set(vidObj,'FrameRate',fps);
                open(vidObj);
                for it = 1:length(its)                
                    for ch = 1:length(chns)
                        filepath = frameset{s,xy,ch,it}.filepath;
                        temp = load(filepath);
                        img = double(temp.img-minimg(ch))./maximg(ch);
                        tcolor = cat(3,img,img,img);
                        axes(ha(ch));
                        image(tcolor);
                        set(ha(ch),'xtick',[]);
                        set(ha(ch),'ytick',[]);
                        title([chns{ch},' (',num2str(it),')']);
                    end
                    writeVideo(vidObj,getframe(gcf));
                end
                close(vidObj);
                close(fig);
            end
        end
    end

end