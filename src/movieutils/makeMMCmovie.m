function [] = makeMMCmovie(name,frameset,chn,xys,params,maximg,minimg,fps)
    
    img = loadmatimg(frameset{1,1,3,1});
    [nx,ny] = size(img);
    xmat = zeros(nx,ny);
    ymat = zeros(nx,ny);
    for x = 1:nx
        for y = 1:ny
            xmat(x,y) = x;
            ymat(x,y) = y;
        end
    end
    tempgrid = zeros(5,5);
    filt = normdiscmat(tempgrid,[0,0],params.sig);
    
    % identify the connected components from the each
    yframepos = {};
    totalccs = 0;
    for xy = xys
        img1  = loadmatimg(frameset{1,xy,3,1});
        img1  = rotateimg(img1,params.rot);
        img2  = loadmatimg(frameset{1,xy,2,1});
        img2  = rotateimg(img2,params.rot);
        img3  = zeros(size(img1)); 
        img3(:) = max([img1(:)';img2(:)']);
        % remove some of the noise
        fimg = imfilter(img3,filt,'replicate','conv');
        % get chambers based on fluo thresholding
        [stats,~] = mmgetcomponents(fimg,xmat,ymat,params.minints,params.maxpixs);
        % pick the center for each
        ycenters  = zeros(1,length(stats));
        for cc = 1:length(stats)
            pixs = stats(cc).PixelIdxList;
            ycenters(cc) = floor(mean(ymat(pixs)));
        end
        % remove duplicates
        added     = zeros(size(ycenters));
        dmatrix   = ycenters-ycenters';
        fcents    = 0;
        ycenters2 = [];
        for cc = 1:length(stats)
            if(added(cc)==1)
                continue;
            end
            if(length(stats(cc).PixelIdxList)<50)
                % skip boxes smaller than 50 
                added(cc) = 1;
                continue;
            end
            same = find(dmatrix(cc,cc:end)<15);
            added(cc) = 1;
            fcents = fcents+1;
            ycenters2 = [ycenters2,floor(mean(ycenters(same+cc-1)))];
            if(length(same)>1) % mark other as added
                for s = [same(2:end)+cc-1]
                    added(s) = 1;
                end
            end
        end
        yframepos{xy} = ycenters2;
        totalccs = totalccs+length(ycenters2);
    end
    display(['Total number of chambers ',num2str(totalccs)]);
    
    x0 = 320;
    x1 = 500;
    dy = 21;
    dx = length(x0:x1);
    ndx = 1;
    if(totalccs<=60)
        ndx = 1;
    elseif(totalccs>60 && totalccs<=120)
        ndx = 2;
    elseif(totalccs>120 && totalccs<=180)
        ndx = 3;
    elseif(totalccs>180 && totalccs<=240)
        ndx = 4;
    else
        error('Too many. Not implemented');
    end
    prefix = frameset{1,1,1,1}.prefix;
    [~,~,~,its] = size(frameset);
    fig     = figure('MenuBar','none','Position',[0,0,1300,dx*ndx+40]);
    ha      = axes('Units','Pixels','Position',[10,3,dy*60,dx*ndx]);
    vidObj = VideoWriter([name,'.avi']);
    set(vidObj,'FrameRate',fps);
    open(vidObj);
    for it = 1:its
        currcc = 0;
        if(length(chn)==1)
            plotimg = zeros(ndx*dx,dy*60);
            for xy = xys
                [img,dtime] = loadmatimg(frameset{1,xy,chn,it});
                img  = rotateimg(img,params.rot);
                ycenters    = yframepos{xy};
                for cc = 1:length(ycenters)
                    y0 = ycenters(cc)-(dy-1)/2;
                    y1 = ycenters(cc)+(dy-1)/2;
                    if(y0<1)
                        y0 = 1;
                        y1 = dy;
                    end
                    if(y1>ny)
                        y0 = ny-dy+1;
                        y1 = ny;
                    end
                    simg = img(x0:x1,y0:y1);
                    currcc = currcc+1;
                    if(currcc<=60)
                        plotimg(1:dx,(dy*(currcc-1)+1):(dy*(currcc))) = simg;
                    elseif(currcc<=120)
                        plotimg(dx+1:2*dx,(dy*(currcc-61)+1):(dy*(currcc-60))) = simg;
                    elseif(currcc<=180)
                        plotimg((2*dx+1):(3*dx),(dy*(currcc-121)+1):(dy*(currcc-120))) = simg;
                    elseif(currcc<=240)
                        plotimg((3*dx+1):end,(dy*(currcc-181)+1):(dy*(currcc-180))) = simg;
                    end
                end
            end
            plotimg = (plotimg-minimg)./maximg;
            tcolor = cat(3,plotimg,plotimg,plotimg);
        else
            plotimg1 = zeros(ndx*dx,dy*60);
            plotimg2 = zeros(ndx*dx,dy*60);
            for xy = xys
                [img1,dtime] = loadmatimg(frameset{1,xy,chn(1),it});
                [img2,dtime] = loadmatimg(frameset{1,xy,chn(2),it});
                img1  = rotateimg(img1,params.rot);
                img2  = rotateimg(img2,params.rot);
                ycenters    = yframepos{xy};
                for cc = 1:length(ycenters)
                    y0 = ycenters(cc)-(dy-1)/2;
                    y1 = ycenters(cc)+(dy-1)/2;
                    if(y0<1)
                        y0 = 1;
                        y1 = dy;
                    end
                    if(y1>ny)
                        y0 = ny-dy+1;
                        y1 = ny;
                    end
                    simg1 = img1(x0:x1,y0:y1);
                    simg2 = img2(x0:x1,y0:y1);
                    currcc = currcc+1;
                    if(currcc<=60)
                        plotimg1(1:dx,(dy*(currcc-1)+1):(dy*(currcc))) = simg1;
                        plotimg2(1:dx,(dy*(currcc-1)+1):(dy*(currcc))) = simg2;
                    elseif(currcc<=120)
                        plotimg1(dx+1:2*dx,(dy*(currcc-61)+1):(dy*(currcc-60))) = simg1;
                        plotimg2(dx+1:2*dx,(dy*(currcc-61)+1):(dy*(currcc-60))) = simg2;
                    elseif(currcc<=180)
                        plotimg1((2*dx+1):(3*dx),(dy*(currcc-121)+1):(dy*(currcc-120))) = simg1;
                        plotimg2((2*dx+1):(3*dx),(dy*(currcc-121)+1):(dy*(currcc-120))) = simg2;
                    elseif(currcc<=240)
                        plotimg1((3*dx+1):end,(dy*(currcc-181)+1):(dy*(currcc-180))) = simg1;
                        plotimg2((3*dx+1):end,(dy*(currcc-181)+1):(dy*(currcc-180))) = simg2;
                    end
                end
            end
            plotimg1 = (plotimg1-minimg(1))./maximg(1);
            plotimg2 = (plotimg2-minimg(2))./maximg(2);
            temp = zeros(size(plotimg2));
            tcolor = cat(3,plotimg2,plotimg1,temp);
        end
        image(tcolor);
        set(ha,'xtick',[]);
        set(ha,'ytick',[]);
        title([num2str(floor(dtime/60)),' min']);
        writeVideo(vidObj,getframe(gcf));
    end
    close(vidObj);
    close(fig);
end