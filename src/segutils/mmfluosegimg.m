function [cells,mask0] = mmfluosegimg(img,params)
    
    sig       = params.sig;
    lfilt     = params.lfilt;
    widref    = params.widref;
    lcut      = params.lcut;
    minints   = params.minints;
    maxpixs   = params.maxpixs;
    fitsdist  = params.fitsdist;
    deltapix  = params.deltapix;
    addtips   = params.addtips;
    addextra  = params.addextra;
    minlength = params.minlength;
    scmethod  = params.scmethod;
    helpfig   = params.helpfig;
    
    [nx,ny] = size(img);
    xmat = zeros(nx,ny);
    ymat = zeros(nx,ny);
    for x = 1:nx
        for y = 1:ny
            xmat(x,y) = x;
            ymat(x,y) = y;
        end
    end
    
    tempgrid = zeros(5,5);
    filt = normdiscmat(tempgrid,[0,0],sig);

    % remove some of the noise
    fimg = imfilter(img,filt,'replicate','conv');
    % get chambers based on fluo thresholding
    [stats,mask0] = mmgetcomponents(fimg,xmat,ymat,minints,maxpixs);
% $$$     figure();
% $$$     imshow(mask0,[]);
    % go and find cells for each chamber
    cells  = {};
    ncells = 0;
    for cc = 1:length(stats)
        pixs = stats(cc).PixelIdxList;
        if(length(pixs)<50)
            continue;
        end
        % get a box that contains all cells
        [box] = getbox(pixs,xmat,ymat,deltapix);
        simg  = fimg(box(1):box(2),box(3):box(4));
        edges = zeros(size(simg,1),2); % location of edges
        maxi  = zeros(size(simg,1),1); % max location (mid edges)
        toti  = zeros(size(simg,1),1); % integral of intensity
        maxv  = zeros(size(simg,1),1); % 
        for x = 1:size(simg,1)
            % iterate over cross section
            vals = simg(x,:);
            [refval,~] = max(vals);
            tval = refval*widref;
            % find edges, max location and sum intent bewteen edges
            [edge,maxlocat,totalint] = findedges(vals,tval);
            edges(x,1:2) = edge;
            maxi(x)      = maxlocat;
            toti(x)      = totalint;
            if(~isnan(maxlocat))
                maxv(x) = vals(round(maxlocat));
            else
                maxv(x) = NaN;
            end
        end
        if(helpfig==1)
            figure();
            imshow(simg,[]);
            hold on;
            plot(maxi,1:size(edges,1),'r');
            plot(edges(:,2),1:size(edges,1),'g');
            plot(edges(:,1),1:size(edges,1),'g');
        end
        
        % low pass filter (TODO: try different ones)
        if(scmethod==0)
            lscores = imfilter(maxv,lfilt,'replicate','conv')./maxv;
            rmvls   = find(lscores<lcut); 
        elseif(scmethod==1)
            pps = find(isnan(maxv)==0);
            svals = smooth(maxv(pps),5);
            vals = (svals-maxv(pps))./svals;
            lscores = NaN*zeros(size(maxv));
            lscores(pps) = vals;
            rmvls   = find(lscores>lcut); %lcut
        else
            error('WTH!');
        end
        %lscores = imfilter(toti,lfilt,'replicate','conv')./toti;
        maxi(rmvls) = NaN;
        % contigous values of not NaN will called cells
        goodps  = ~isnan(maxi);
        p0 = 1;
        clcells = []; % map for cells in clusters
        clcount = 0; % cells in clusters count
                     % find the next value not NaN and where it ends    
        if(helpfig==2)
            figure();
% $$$             subplot(3,1,1); plot(toti);
% $$$             axis([1 length(toti) min(toti) max(toti)]);
            subplot(3,1,1); plot(maxv);
            axis([1 length(maxv) min(maxv) max(maxv)]);
            subplot(3,1,2); hold on; 
            %plot(lscores); plot(1:length(lscores),lcut*ones(size(lscores)),'r');
            %vals = -1*maxv./(max(maxv));
            [pps] = find(isnan(maxv)==0);
            svals = smooth(maxv(pps),5);
            vals = (svals-maxv(pps))./svals;
            %vals = -1*maxv;
            xs = 1:length(maxv);
            plot(xs(pps),vals);
% $$$             [~,locs] = findpeaks(vals,...
% $$$                                  'MinPeakDistance',5,...
% $$$                                  'MinPeakWidth',3,...
% $$$                                  'MinPeakProminence',0.01);
% $$$             locs = locs;
% $$$             for l = locs'
% $$$                 plot([l+pps(1)-1,l+pps(1)-1],[vals(l),0],'r');
% $$$             end
            plot(1:length(maxv),0.05*ones(size(maxv)),'r');
            axis([1 length(xs) min(vals) max(vals)]);
            subplot(3,1,3); hold on; 
            plot((1-goodps).*lscores); 
            plot(1:length(lscores),lcut*ones(size(lscores)),'r');
            axis([1 length(lscores) min((1-goodps).*lscores) 0]);
        end
        if(helpfig==4)
            figure();
            subplot(3,1,1); 
            plot(1:size(edges,1),toti);
            subplot(3,1,2); hold on; 
            plot(1:size(edges,1),maxv);
            subplot(3,1,3); hold on; 
            plot(1:size(edges,1),edges(:,2)-edges(:,1));
        end
        if(helpfig==1)
            figure();
            imshow(simg,[]);
            hold on;
            plot(maxi,1:size(edges,1),'r');
            plot(edges(:,2),1:size(edges,1),'g');
            plot(edges(:,1),1:size(edges,1),'g');
        end
        while(sum(goodps(p0:end))>0)            
            nexts = find(goodps(p0:end)==1);
            p0 = p0+nexts(1)-1;
            ends  = find(goodps(p0:end)==0);
            if(isempty(ends)) 
                % half a cell in the end
                p0 = length(goodps)+1;
                continue;
            end
            p1 = p0+ends(1)-2;
            if(p1<=p0) % just in case
                p0 = p1+2;
                continue;
            end
            % in case there is a cell above
            if(p0<deltapix)
                p0 = p1+2;
                continue;
            end
            % remove small things right away
            if((p1-p0)<minlength)
                p0 = p1+2;
                continue;
            end
            % Just in case remove rows that have low width
            widths = edges(p0:p1,2)-edges(p0:p1,1);
% $$$             rtoti = toti(p0:p1)./mean(toti(p0:p1));
% $$$             tokeep = find(rtoti>=0.85);
            if(median(widths)==0)
                p0 = p1+2;
                continue;
            end
            tokeep = find((widths./median(widths))<1.1);
            if(tokeep(end)<length([p0:p1]))
                p1 = p1-(length([p0:p1])-tokeep(end));
            end
            if(tokeep(1)>1)
                p0 = p0+min([100,tokeep(1)-1]);
            end
            
            % Safety check
            if(isnan(maxi(p0)))
                error('maxi(p0) cannot be NaN');
            end
            if(isnan(maxi(p1)))
                error('maxi(p1) cannot be NaN');
            end

            % Width and center will be estimated from these values
            width   = mean(edges(p0:p1,2)-edges(p0:p1,1));
            center  = [mean(maxi(p0:p1))+box(3)-1,mean([p0:p1])+box(1)-1];
            
            % skip cells that have way biger width than length
            if(width/2>length(p0:p1))
                p0 = p1+2;
                continue;
            end
            % do a fit of intensity profile in the center
            k = floor((p1+p0)/2);
            dvals  = (simg(k,:)-min(simg(k,:)))./(max(simg(k,:))-min(simg(k,:)));
            intfit = fit([-40,1:size(simg,2),40]'-maxi(k),...
                         [0,dvals,0]',...
                         'smoothingspline');
            % init cell struct
            ncells  = ncells+1;
            clcount = clcount+1;
            clcells(clcount) = ncells;
            cells{ncells}         = struct;
            cells{ncells}.p0      = p0;        
            cells{ncells}.p1      = p1;
            cells{ncells}.rval0   = simg(p0,floor(maxi(p0)));
            cells{ncells}.rpos0   = [p0,maxi(p0)];
            cells{ncells}.rbkg0   = min(simg(p0,:));
            cells{ncells}.rval1   = simg(p1,floor(maxi(p1)));
            cells{ncells}.rpos1   = [p1,maxi(p1)];
            cells{ncells}.rbkg1   = min(simg(p1,:));
            cells{ncells}.width   = width;
            cells{ncells}.center  = center;
            cells{ncells}.intfit  = intfit;
            
            if(helpfig==3)
                hold on;
                plot(maxi(p0:p1),p0:p1,'r');
                plot(edges(p0:p1,1),p0:p1,'g');
                plot(edges(p0:p1,2),p0:p1,'g');
            end
            p0 = p1+2; % move to next one
        end

        xposs = 1:size(simg,1);
        sxmat = repmat([1:size(simg,1)]',1,size(simg,2));
        symat = repmat([1:size(simg,2)],size(simg,1),1);
        % Correct the tips and do final computations...
        % The point is to fix p0 and p1, and also change xposs
        % for the cell tips
        for c = 1:clcount
            cID = clcells(c);
            reffit = cells{clcells(1)}.intfit;
            % get reference values of up and down cells
            uprval = 0; uprpos = [0,0]; uprbkg = 0;
            dwrval = 0; dwrpos = [0,0]; dwrbkg = 0;
            % Make an intensity image removing effect from tob and bottom cells
            rsimg  = simg;
            if(c>1)             
                % There is a cell on top
                prval = cells{clcells(c-1)}.rval1;
                prpos = cells{clcells(c-1)}.rpos1;
                prbkg = cells{clcells(c-1)}.rbkg1;
            else
                prpos = [1,0];
                [prval,prpos(2)] = max(simg(1,:));
                prbkg = cells{clcells(c)}.rbkg0;
            end
            rval = cells{clcells(c)}.rval0;
            rpos = cells{clcells(c)}.rpos0;
            rbkg = cells{clcells(c)}.rbkg0;
            % Let's build a matrix with all distances from uprpos
            sdist  = sqrt((sxmat-prpos(1)).^2+(symat-prpos(2)).^2);
            % and estimate how much should be the relative
            % contribution from uprpos within a certain radius
            temp   = zeros(size(sdist));
            repls  = find(abs(sdist)<=fitsdist);
            temp(repls) = reffit(sdist(repls));
            rsimg  = rsimg - (prval-prbkg).*temp;
            % I now this is not the proper way to do it, 
            % but for now let's go along with it.
            rsimg(1:(prpos(1)-1),:) = prbkg;
            if(c<clcount)
                % There is a cell on bottom
                prval = cells{clcells(c+1)}.rval0;
                prpos = cells{clcells(c+1)}.rpos0;
                prbkg = cells{clcells(c+1)}.rbkg0;
            else
                prpos = [size(simg,1),0];
                [prval,prpos(2)] = max(simg(end,:));
                prbkg = cells{clcells(c)}.rbkg1;
            end
            rval = cells{clcells(c)}.rval1;
            rpos = cells{clcells(c)}.rpos1;
            rbkg = cells{clcells(c)}.rbkg1;
            sdist  = sqrt((sxmat-prpos(1)).^2+(symat-prpos(2)).^2);
            temp   = zeros(size(sdist));
            repls  = find(abs(sdist)<=fitsdist);
            temp(repls) = reffit(sdist(repls));
            rsimg  = rsimg - (prval-prbkg).*temp;
            rsimg((prpos(1)+1):end,:) = prbkg;
% $$$             subplot(1,3,3);
% $$$             imshow(rsimg,[]);
            % Fix top and bottom
            p0    = cells{cID}.p0;
            p1    = cells{cID}.p1;
            p01   = max([1,(p0-deltapix)]);
            p02   = max([1,(p0-1)]);
            p11   = min([size(simg,1),(p1+1)]);
            p12   = min([size(simg,1),(p1+deltapix)]);
            xs    = [p01:p02,p11:p12];
            tvals = [max(simg(p0,:))*ones(size(1:8)),...
                     max(simg(p1,:))*ones(size(1:8))];
            tvals = tvals.*widref;
            if(addextra==1)
                for k = 1:length(xs)
                    x = xs(k);
                    vals = rsimg(x,:);
                    % find edges, max location and sum intent bewteen edges
                    [edge,maxlocat] = findedges(vals,tvals(k));
                    if(~isnan(maxlocat))
                        edges(x,1:2)    = edge;
                        maxi(x)         = maxlocat;
                        if((k<=8)&&(x<p0))
                            p0 = x;
                        elseif((k>8)&&(x>p1))
                            p1 = x;
                        end
                    end
                end
            end
            
            % flat the edges
            for x = p0:p1
                if(~(edges(x,2)==edges(x,1)) && ...
                   (edges(x,2)-edges(x,1))>cells{cID}.width)
                    edges(x,2) = maxi(x)+cells{cID}.width/2;
                    edges(x,1) = maxi(x)-cells{cID}.width/2;
                end
            end
            
            % add tips if needed
            if(addtips==1)
                if(~(edges(p0,1)==edges(p0,2)))
                    if(p0>1)
                        p0 = p0-1;
                        edges(p0,1) = maxi(p0+1);
                        edges(p0,2) = maxi(p0+1);
                        maxi(p0)    = maxi(p0+1);
                    end
                end
                if(~(edges(p1,1)==edges(p1,2)))
                    if(p1<length(maxi))
                        p1 = p1+1;
                        edges(p1,1) = maxi(p1-1);
                        edges(p1,2) = maxi(p1-1);
                        maxi(p1)    = maxi(p1-1);
                    end
                end
                % correct xpos for tips
                ps = [p0,p1];
                ss = [1,-1];
                ts = [tvals(1),tvals(end)];
                for k = 1:2
                    p  = ps(k);
                    vi = rsimg(p,round(edges(p,1)));
                    vo = rsimg(p+ss(k),round(edges(p,1)));
                    if((vi<ts(k))&&(vo>vi))
                        d  = (vo-ts(k))./(vo-vi);
                        % just in case
                        if(d<0) 
                            d = 0;
                        end
                        if(d>1)
                            d = 1;
                        end
                        xposs(p) = xposs(p+ss(k))-ss(k)*d;
                    else
                        error('');
                    end
                end
            end
            % fix NaN in bewteen
            if(sum(isnan(maxi(p0:p1)))>0)
                tofix = [];
                lastedge = [];
                for p = p0:p1
                    if(isnan(maxi(p)))
                        tofix = [tofix,p];
                    else
                        if(~isempty(tofix))
                            edges(tofix,1) = (lastedge(1)+edges(p,1))/2;
                            edges(tofix,2) = (lastedge(2)+edges(p,2))/2;
                            maxi(tofix)    = (edges(tofix,1)+edges(tofix,2))/2;
                        end
                        tofix = [];
                        lastedge = edges(p,:);
                    end
                end
            end
            
            % Final cell struct
            midline = [maxi(p0:p1),xposs(p0:p1)'];
            border  = [[xposs(p0:p1)',edges(p0:p1,1)];...
                       [flipud(xposs(p0:p1)'),flipud(edges(p0:p1,2))]];
            midline(:,1) = midline(:,1)+box(3)-1;
            midline(:,2) = midline(:,2)+box(1)-1;
            border(:,1)  = border(:,1)+box(1)-1;
            border(:,2)  = border(:,2)+box(3)-1;
            
            cells{cID}.midline = midline;
            cells{cID}.border  = border;
            cells{cID}.edges   = edges(p0:p1,:)+box(3)-1;
            cells{cID}.length  = midline2length(midline);
            cells{cID}.area    = border2area(cells{cID});
            cells{cID}.p0      = p0;
            cells{cID}.p1      = p1;
            cells{cID}.PixelIdxList = border2pixels(img,cells{cID});
            cells{cID}.angle  = midline2angle(midline);
        end
    end
    
    
    function [edge,maxlocat,totalint] = findedges(values,ref)
        edge = [NaN,NaN];
        totalint = 0;
        maxlocat = NaN;
        valsp = find(values>=ref);
        if(length(valsp)==1) % there is only one pixel
            edge(1:2)  = valsp(1);
            maxlocat   = valsp(1);
        elseif((length(valsp)>1)&&...
               ~(valsp(1)==1) && ...
               ~(valsp(end)==length(values)))
            v1  = values(valsp(1));
            vd  = values(valsp(1)-1);
            d1  = min([1,abs((v1-tval)./(v1-vd))]);
            edge(1) = valsp(1)-d1;
            
            v2  = values(valsp(end));
            vd = values(valsp(end)+1);
            d2  = min([1,abs((v2-tval)./(v2-vd))]);
            edge(2) = valsp(end)+d2;
            
            maxlocat = (edge(1)+edge(2))/2;
            totalint = sum(values(valsp))+d1*v1+d2*v2;
        else
            nvals2  = floor(length(vals)/2);
            totalint = sum(values(max([1,nvals2-3]):min([length(vals),nvals2+3])));
        end
    end

end