function [score] = getPSEGscore(img,filts,angles,na)
    [nx,ny] = size(img);
    fimgs = zeros(nx,ny,na);
    for i = 1:na
        fimgs(:,:,i) = imfilter(img,-1*filts{i},'replicate','conv');
    end
    fimgs2 = fimgs(:,:,1:na/2)+fimgs(:,:,na/2+1:end);
    score    = zeros(nx,ny);
    score(:) = sum(reshape(fimgs2.*real(acos(pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
    norms    = zeros(nx,ny);
    norms(:) = sum(reshape(fimgs2.*real(acos(-1*pi*(fimgs2./abs(fimgs2)))./pi),nx*ny,na/2)');
    score    = score./(norms+1);
    score    = exp(imcomplement(log(-1*score+1)))./log(10);
% $$$     tempgrid = zeros(7,7);
% $$$     filt2 = normdiscmat(tempgrid,[0,0],0.25);
% $$$     filt  = 10*normdiscmat(tempgrid,[0,0],sig1)-1*normdiscmat(tempgrid,[0,0],sig2);
% $$$     J     = deconvlucy(img,filt);
% $$$     score = 10*J./(imfilter(img,filt2,'replicate','conv'));
end