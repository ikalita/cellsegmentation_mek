function [frameset] = reconcileSeg2Chns(frameset,chn1,chn2,maxdist,minintc);

[nsteps,nxys,nchns,nits] = size(frameset);
for st = 1:nsteps
    for xy = 1:nxys
        for it = 1:nits
            cells1 = frameset{st,xy,chn1,it}.cells;
            cells2 = frameset{st,xy,chn2,it}.cells;
            nc1 = length(cells1);
            nc2 = length(cells2);
            if(nc1>0 && nc2==0) % no cells in 2
                frameset{st,xy,chn2,it}.cells = cells1;
                frameset{st,xy,chn2,it}.selectedcells = ones(size(cells1));
            elseif(nc1==0 && nc2>0) % no cells in 1
                frameset{st,xy,chn1,it}.cells = cells2;
                frameset{st,xy,chn1,it}.selectedcells = ones(size(cells2));
            elseif(nc1==0 && nc2==0) % no cells in either
            else
                temp = load(frameset{st,xy,chn1,it}.filepath);
                img1 = double(temp.img);
                temp = load(frameset{st,xy,chn2,it}.filepath);
                img2 = double(temp.img);
                [cands2to1] = mapCellFrames(cells1,cells2,maxdist,minintc);
                [cands1to2] = mapCellFrames(cells2,cells1,maxdist,minintc);
                % Build a matrix with all intersections
                % not very efficient, but does the trick
                pairs = zeros(nc1,nc2);
                for c2 = 1:nc2
                    cands = cands2to1{c2};
                    if(~isempty(cands))
                        for c1 = cands(:,1)
                            pairs(c1,c2) = 1;
                        end
                    end
                end
                for c1 = 1:nc1
                    cands = cands1to2{c1};
                    if(~isempty(cands))
                        for c2 = cands(:,1)
                            pairs(c1,c2) = 1;
                        end
                    end
                end
                % TODO: check for inconsistencies in the mapping
                % e.g minintc is a relative cut, so it is not simetric

                usedin1 = zeros(nc1,1);
                usedin2 = zeros(nc2,1);
                newcells = {};
                count = 0;
                for c1 = 1:nc1
                    if(usedin1(c1)==0)
                        imapin1 = c1;
                        % find maps to cell2
                        imapin2 = find(pairs(c1,:)==1);
                        if(isempty(imapin2))
                            count = count+1;
                            newcells{count}  = cells1{c1};
                            usedin1(imapin1) = 1;
                        else
                            % see if more from c1 need to be added
                            for i2 = 1:length(imapin2)
                                c2 = imapin2(i2);
                                addto1 = find(pairs(:,c2)==1);
                                for i1 = 1:length(addto1)
                                    if((~isempty(find(imapin1==addto1(i1))))&&...
                                       (usedin1(addto1(i1))>0))
                                        imapin1 = [imapin1,addto1(i1)];
                                    end
                                end
                            end
                            mvals1 = zeros(length(imapin1),1);
                            for i1 = 1:length(imapin1)
                                mvals1(i1) = mean(img1(cells1{imapin1(i1)}.PixelIdxList));
                            end
                            mvals2 = zeros(length(imapin2),1);
                            for i2 = 1:length(imapin2)
                                mvals2(i2) = mean(img2(cells2{imapin2(i2)}.PixelIdxList));
                            end
                            if(mean(mvals1)>mean(mvals2))
                                for i1 = 1:length(imapin1)
                                    count = count+1;
                                    newcells{count}  = cells1{imapin1(i1)};
                                end
                            else
                                for i2 = 1:length(imapin2)
                                    count = count+1;
                                    newcells{count}  = cells2{imapin2(i2)};
                                end
                            end
                            usedin1(imapin1) = 1;
                            usedin2(imapin2) = 1;
                        end
                    end
                end
                rem2 = find(usedin2==0);
                if(~isempty(rem2))
                    for i2 = 1:length(rem2)
                        c2 = rem2(i2);
                        count = count+1;
                        newcells{count}  = cells2{c2};
                        usedin1(imapin1) = 1;
                    end
                end
                frameset{st,xy,chn2,it}.cells = newcells;
                frameset{st,xy,chn1,it}.cells = newcells;
                frameset{st,xy,chn2,it}.selectedcells = ones(size(newcells));
                frameset{st,xy,chn1,it}.selectedcells = ones(size(newcells));
            end
        end
    end
end

end