function [angle] = midline2angle(midl)

pfit = fit(midl(:,1),midl(:,2),'poly1');

angle = 90*atan(pfit.p1)/(pi/2);

end 