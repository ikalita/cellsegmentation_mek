function [] = resaveFrameset(outfile,frameset,prevstruct);

lastversion = prevstruct.('lastversion');
date        = prevstruct.('date');
prefix      = prevstruct.('prefix');
FSEGfiltpop = prevstruct.('FSEGfiltpop');
steps       = prevstruct.('steps');
xys         = prevstruct.('xys');
chns        = prevstruct.('chns');
its         = prevstruct.('its');
if(isfield(prevstruct,'filts'))
    filts       = prevstruct.('filts');
else
    filts = [];
end
if(isfield(prevstruct,'angles'))
    angles      = prevstruct.('angles');
else
    angles = [];
end
if(isfield(prevstruct,'na'))
    na          = prevstruct.('na');
else
    na = [];
end
if(isfield(prevstruct,'ptable'))
    ptable = prevstruct.('ptable');
else
    ptable = [];
end

save(outfile,...
     'frameset','FSEGfiltpop','angles','chns','date','filts',...
     'its','lastversion','na',...
     'prefix','ptable','steps','xys');

end