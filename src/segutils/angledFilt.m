function [filts,na,angles] = angledFilt(nangles,dmax,sig)

angles = linspace(0,180,nangles);
angles1 = angles(1:end-1);
angles2 = [angles(end),angles(2:end-1)+180];
angles = [angles1,angles2];

na = length(angles);
ngrid = 2*(dmax-1)+1;

%% make my own filters
filts = cell(length(angles),1);
for a = 1:na
    angle = angles(a);
    tempgrid = zeros(ngrid,ngrid);
    resmat = zeros(ngrid,ngrid);
    for i = 1:dmax
        d = i;
        x = cos(pi*angle/180)*d;
        y = sin(pi*angle/180)*d;
        resmat = resmat+normdiscmat(tempgrid,[x,y],sig);
    end
    srmat = sum2(resmat);
    resmat = -1*resmat + srmat.*normdiscmat(tempgrid,[0,0],sig);
    filts{a} = resmat;
end

end