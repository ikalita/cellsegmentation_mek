function [mcell] = mmmergecells(cell1,cell2)

% Cells have these fields
% -> midline, border, edges (ignore others)
% It will merge the two cells by adding the midlines, and then
% completing the borders using the average width. Nothing fancy.

xpos1 = cell1.midline(:,2);
xpos2 = cell2.midline(:,2);

mids1 = cell1.midline(:,1);
mids2 = cell2.midline(:,1);

edge1 = cell1.edges;
edge2 = cell2.edges;

mcell = struct;
mcell.center  = (cell1.center+cell2.center)/2;
mcell.width   = (cell1.width+cell2.width)/2;

if(~isempty(intersect(xpos2,xpos1)))
    error('');
end

x0 = 0;
if(xpos1(1)>xpos2(2)) % first cell on bottom
    xpos  = [xpos2;xpos1];
    mids  = [mids2;mids1];
    edge  = [edge1;edge2];
    x0    = length(xpos2);
else
    xpos  = [xpos1;xpos2];
    mids  = [mids1;mids2];
    edge  = [edge1;edge2];
    x0    = length(xpos1);
end

xmin = max([1,x0-10]);
xmax = min([size(edge,1),x0+10]);

for n = xmin:xmax
    w = edge(n,2)-edge(n,1);
    if(w<(mcell.width*0.8))
        edge(n,1) = mids(n)-mcell.width/2;
        edge(n,2) = mids(n)+mcell.width/2;
    end
end

border  = [[xpos,edge(:,1)];...
           [flipud(xpos),flipud(edge(:,2))]];

mcell.midline = [mids,xpos];
mcell.edges   = edge;
mcell.border  = border;
mcell.length  = midline2length(mcell.midline);
mcell.area    = border2area(mcell);
%mcell.PixelIdxList = border2pixels(img,cells{cID});
mcell.angle  = midline2angle(mcell.midline);

% TODO: PixelIdxList


end