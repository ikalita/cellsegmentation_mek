function [intl,clen] = midline2length(midl)
% computes the path integral of midl

% get differences
ds1 = midl(1:end-1,:)-midl(2:end,:);
% get sqrt .^2 sums
ds2 = sqrt(ds1(:,1).^2+ds1(:,2).^2);
% add it up!
intl = sum(ds2);
clen = cumsum(ds2);


end 