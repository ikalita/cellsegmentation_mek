function [stats,mask0] = mmgetcomponents(img,xmat,ymat,minints,maxpixs)

mask0 = mytheimage(img,minints);
mask0 = bwareaopen(mask0,maxpixs);

statprops = {'PixelIdxList'};
ccs    = bwconncomp(mask0);
stats  = regionprops(ccs,statprops{:});

[nx,ny]    = size(img);
for cc = 1:length(stats)
    pixs = stats(cc).PixelIdxList;
    temp       = zeros(size(img));
    temp(pixs) = 1;
    pixpos = find(temp==1);
    xtemp  = temp.*xmat;
    vals   = max(xtemp)-min(xtemp+1000*(1-temp));
    pos1   = find(vals>0);
    pos2   = find(vals(pos1)<0.3*max(vals(pos1)));
    if(~isempty(pos2))        
        for y = pos1(pos2)
            pps = find(temp(:,y)==1);
            [xmin] = min(xtemp(pps,y));
            [xmax] = max(xtemp(pps,y));
            mask0(xmin:xmax,y) = 0;
        end
    end
end

ccs    = bwconncomp(mask0);
stats  = regionprops(ccs,statprops{:});

end