function [frameset] = changeSetFolder(frameset,matchstr,replstr)

[nsteps,nxys,nchns,nits] = size(frameset);
for st = 1:nsteps
    for xy = 1:nxys
        for it = 1:nits
            for ch = 1:nchns
                folder = frameset{st,xy,ch,it}.folder;
                folder = regexprep(folder,matchstr,replstr);
                folder = regexprep(folder,'\','/');
                folder = regexprep(folder,'\','/');
                frameset{st,xy,ch,it}.folder = folder;
                frameset{st,xy,ch,it}.filepath = [folder,frameset{st,xy,ch,it}.filename];
            end
        end
    end
end

end