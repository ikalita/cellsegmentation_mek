function [frameset] = fixcellsbyint(frameset,segchns,sig,dupix,rfrac)

tempgrid = zeros(5,5);
filt = normdiscmat(tempgrid,[0,0],sig);

xmat = zeros(512,512);
ymat = zeros(512,512);
for x = 1:512
    for y = 1:512
        xmat(x,y) = x;
        ymat(x,y) = y;
    end
end

fixall = 1;

[nsteps,nxys,nchns,nits] = size(frameset);
for st = 1:nsteps
    for xy = 1:nxys
        for it = 1:nits
            for chn = segchns
                cells = frameset{st,xy,chn,it}.cells;
                nc    = length(cells);
                if(nc>0)
                    temp = load(frameset{st,xy,chn,it}.filepath);
                    img  = double(temp.img);
                    fimg = imfilter(img,filt,'replicate','conv');
                    for c = 1:nc
                        csrc     = cells{c};
                        box      = [0,0,0,0]; % x0,y0,x1,y1
                        box(1)   = max([1,min(xmat(csrc.PixelIdxList))]);
                        box(2)   = max([1,min(ymat(csrc.PixelIdxList))-dupix]);
                        box(3)   = min([512,max(xmat(csrc.PixelIdxList))]);
                        box(4)   = min([512,max(ymat(csrc.PixelIdxList))+dupix]);
                        svals    = sort(fimg(csrc.PixelIdxList),'descend');
                        refval   = mean(svals(1:5))
% $$$                         boxvals  = fimg(box(1):box(3),box(2):box(4));
% $$$                         boxvalsp = find(boxvals>=refval*rfrac);
% $$$                         temp = zeros(size(boxvals));
% $$$                         temp(boxvalsp) = 1;
% $$$                         mask = zeros(512,512);
% $$$                         mask(box(1):box(3),box(2):box(4)) = temp;
% $$$                         newcells = mask2cells(mask);
% $$$                         if(length(newcells)==0)
% $$$                             error('Choose a bigger cut perhaps?');
% $$$                         elseif(length(newcells)==1)
% $$$                             cells{c} = newcells{1};
% $$$                         else
% $$$                             candsai = zeros(length(newcells),1);
% $$$                             for cc = 1:length(newcells)
% $$$                                 candsai(cc) = areaintersect(csrc,newcells{cc});
% $$$                             end
% $$$                             [~,bestc] = max(candsai);
% $$$                             cells{c} = newcells{bestc};
% $$$                         end
                        xvec = box(1):box(3);
                        yvec = box(2):box(4);
                        edges = zeros(length(xvec),2);
                        midvs = zeros(length(xvec),1);
                        for n = 1:length(xvec)
                            vals  = fimg(xvec(n),yvec);
                            valsp = find(vals>=refval*rfrac);
                            if(length(valsp)==1)
                                edges(n,1) = yvec(valsp(1));
                                edges(n,2) = yvec(valsp(1));
                                midvs(n)   = fimg(xvec(n),valsp(1));
                            elseif(length(valsp)>1)
                                drat1 = (vals(valsp(1))-refval*rfrac)./(vals(valsp(1))-vals(valsp(1)-1));
                                edges(n,1) = yvec(valsp(1))-drat1;
                                drat2 = (vals(valsp(end))-refval*rfrac)./(vals(valsp(end))-vals(valsp(end)+1));
                                edges(n,2) = yvec(valsp(end))+drat2;
                                midvs(n)   = fimg(xvec(n),round((edges(n,2)+edges(n,1))/2));
                                if(drat1>1||drat1<0||drat2>1||drat2<0)
                                    error('wrong!');
                                end
                            end
                        end
% $$$                         degs  = edges(:,2)-edges(:,1);
% $$$                         svals = sort(degs,'ascend');
% $$$                         width = mean(svals(1:5));
% $$$                         frameset{st,xy,chn,it}.cells{c}.width = width;
% $$$                         midvs./refval
% $$$                         rmvp = find(midvs./refval<rfrac);
% $$$                         if(length(rmvp)==0)
% $$$                             figure(); 
% $$$                             imshow(fimg,[]); hold on;
% $$$                             plot(cells{c}.border(:,2),cells{c}.border(:,1),'-');
% $$$                             error('1-Need a bigger cut! cells are too close');
% $$$                         end                            
% $$$                         xvec  = xvec(rmvp(end)+1:end);
% $$$                         edges = edges(rmvp(end)+1:end,:);
% $$$                         midvs = midvs(rmvp(end)+1:end);
% $$$                         rmvp = find(midvs./refval<rfrac);
% $$$                         if(length(rmvp)==0)
% $$$                             figure();
% $$$                             midvs./refval
% $$$                             imshow(fimg,[]); hold on;
% $$$                             plot(cells{c}.border(:,2),cells{c}.border(:,1));
% $$$                             error('2-Need a bigger cut! cells are too close');
% $$$                         end
% $$$                         xvec   = xvec(1:rmvp(1)-1);
% $$$                         edges  = edges(1:rmvp(1)-1,:);
% $$$                         xvecs  = [xvec',xvec'];
% $$$                             
% $$$                             x = xvecs(1,1);
% $$$                             y = ceil(edges(1,1));
% $$$                             d = (fimg(x,y)-refval*rfrac)./(fimg(x,y)-fimg(x-1,y));
% $$$                             xvecs(1,1) = xvecs(1,1)+d;
% $$$                             x = xvecs(end,1);
% $$$                             y = ceil(edges(end,1));
% $$$                             d = (fimg(x,y)-refval*rfrac)./(fimg(x,y)-fimg(x+1,y));
% $$$                             xvecs(end,1) = xvecs(end,1)+d;
% $$$                             x = xvecs(1,2);
% $$$                             y = floor(edges(1,2));
% $$$                             d = (fimg(x,y)-refval*rfrac)./(fimg(x,y)-fimg(x-1,y));
% $$$                             xvecs(1,2) = xvecs(1,2)+d;
% $$$                             x = xvecs(end,2);
% $$$                             y = floor(edges(end,2));
% $$$                             d = (fimg(x,y)-refval*rfrac)./(fimg(x,y)-fimg(x+1,y));
% $$$                             xvecs(end,2) = xvecs(end,2)+d;
% $$$                             
% $$$                             t1 = [xvecs(:,1);flipud(xvecs(:,2))];
% $$$                             t2 = [edges(:,1);flipud(edges(:,2))];
% $$$                             cells{c}.border = [t1,t2];
                    end
                end
            end
        end
    end
end