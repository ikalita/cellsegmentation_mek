function [] = myscatterplot(x1,x2,xmin1,xmax1,xmin2,xmax2,logp)

m1 = mean(x1);
m125 = prctile(x1,25);
m175 = prctile(x1,75);
m2 = mean(x2);
m225 = prctile(x2,25);
m275 = prctile(x2,75);
% $$$ if(logp)
% $$$     xmin1 = log10(xmin1);
% $$$     xmax1 = log10(xmax1);
% $$$     xmin2 = log10(xmin2);
% $$$     xmax2 = log10(xmax2);
% $$$     x1 = log10(x1);
% $$$     x2 = log10(x2);
% $$$     m1 = log10(m1);
% $$$     m2 = log10(m2);
% $$$     m125 = log10(m125);
% $$$     m175 = log10(m175);
% $$$     m225 = log10(m225);
% $$$     m275 = log10(m275);
% $$$ else
% $$$     
% $$$ end

if(logp==1)
    pos  = find(x1>0);
    pos2 = find(x2(pos)>0);
    pos  = pos(pos2);
    
    x1 = x1(pos);
    x2 = x2(pos);
end

if(logp==1)
    dscatter(x1,x2,'logx',1,'logy',1);
else
    dscatter(x1,x2);
end
plot([m1,m1],[xmin2 xmax2],':','Color',[1 0 0],'LineWidth',3);
plot([xmin1 xmax1],[m2,m2],':','Color',[0.2 1 0.2],'LineWidth',3);
axis([xmin1 xmax1 xmin2 xmax2]);
% $$$ if(logp==1)
% $$$     set(gca,'XScale','log');
% $$$     set(gca,'YScale','log');
% $$$ end
grid on;
grid minor;

end