function [linmat,parmat,chlmat,xymat] = lineagematrix(frameset,chn,xy,params)

[~,~,~,nits] = size(frameset);

maps = {};
maps{1} = struct;
for it = 2:nits
    cells1 = frameset{1,xy,chn,it-1}.cells;
    cells2 = frameset{1,xy,chn,it}.cells;
    [addcells,chicells,newcells] = mapCellFrames(cells1,cells2,params);
    map = struct;
    map.addcells = addcells;
    map.chicells = chicells;
    map.newcells = newcells;
    maps{it} = map;
end

linmat = [];
parmat = [];
chlmat = [];
ncells = length(frameset{1,xy,chn,1}.cells);
for c = 1:ncells
    linmat(c,:) = NaN*zeros(1,nits);
    linmat(c,1) = c;
    parmat(c)   = NaN;
    chlmat(c,:) = NaN*zeros(1,nits);
end

for it = 2:nits
    addcells = maps{it}.addcells;
    for n = 1:size(addcells,1);
        c2 = addcells(n,1);
        c1 = addcells(n,2);
        pos = find(linmat(:,it-1)==c1);
        if(length(pos)>1)
            error();        
        elseif(isempty(pos))
            %error(['Could not find cell! IT: ',num2str(it)]);
        elseif(linmat(pos,it)>0)
            display(['In: ',frameset{1,1,1,1}.xy]);
            maps{it}.addcells
            error(['Cell already mapped! IT: ',num2str(it)]);
        else
            linmat(pos,it) = c2;
        end
    end
    chicells = maps{it}.chicells;
    for n = 1:size(chicells,1);
        c2 = chicells(n,1);
        c1 = chicells(n,2);
        newrow     = NaN*zeros(1,nits);
        newrow(it) = c2;
        linmat = [linmat;newrow];
        pos = find(linmat(:,it-1)==c1);
        if(length(pos)>1)
            error(['While making lineage: multiple parent mapping']);
        elseif(isempty(pos))
            display(['WARNING: While making lineage: empty mapping to parent']);
            parmat = [parmat,NaN];
        else
            parmat = [parmat,pos];
        end
        newrow = NaN*zeros(1,nits);
        chlmat = [chlmat;newrow];
        chlmat(pos,it) = size(chlmat,1);
    end
    newcells = maps{it}.newcells;
    for n = 1:length(newcells);
        c2     = newcells(n);
        newrow = NaN*zeros(1,nits);
        newrow(it) = c2;
        linmat = [linmat;newrow];        
        parmat = [parmat,NaN];
        newrow = NaN*zeros(1,nits);
        chlmat = [chlmat;newrow];        
    end
    if(~(size(parmat,2)==size(chlmat,1)))
        error(['Problem while making lineage IT:',num2str(it)]);
    end
end

xymat = {};
for c = 1:(size(linmat,1))
    xymat{c} = [frameset{1,xy,chn,1}.folder,frameset{1,xy,chn,1}.prefix,'_',frameset{1,xy,chn,1}.xy];
end

end