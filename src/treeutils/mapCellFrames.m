function [addcells,chicells,newcells] = mapCellFrames(cells1,cells2,params)
    
    % Map is from cells2 to cells1
    
    deltaclus = params.deltaclus;
    xyorder   = params.xyorder;
    minolap   = params.minolap;
    maxcsum   = params.maxcsum;
    
    nc1 = length(cells1);
    nc2 = length(cells2);
    
    % map clusters
    [prevCLU,prevCLUd] = clusterbypos(cells1,deltaclus,xyorder);
    [currCLU,currCLUd] = clusterbypos(cells2,deltaclus,xyorder);
    cludm = abs(currCLUd(:)-prevCLUd(:)');
    clumap = zeros(size(currCLUd));
    for cl = 1:length(currCLU)
        dists = cludm(cl,:);
        pos = find(dists<deltaclus);
        if(length(pos)>1)
            error('Multiple mapping in clusters');
        elseif(~isempty(pos))
            clumap(cl) = pos(1);
        end
    end
    % clasify into continuation of lineages
    % childs, and new lineages
    addcells  = [[],[],[]];
    chicells  = [[],[],[]];
    newcells  = [];
    prevmaped   = zeros(nc1,1);
    for cl = 1:length(currCLU)
        clu2  = currCLU{cl};
        nclu2 = length(clu2);
        if(clumap(cl)==0)
            for c2 = 1:nclu2
                newcells = [newcells,clu2(c2)];
            end
            continue;                        
        end
        clu1  = prevCLU{clumap(cl)};
        nclu1 = length(clu1);
        y01 = 0; y02 = 0;
        next1 = 1; last2 = 1;
        for c2 = 1:nclu2
            if(last2>c2)
                % skip, was a child
                continue;
            end
            if(next1>nclu1)
                % all cells were tried already
                newcells = [newcells,clu2(c2)];
                last2 = c2+1;
                continue;
            end
            found = 0;            
            for c1 = next1:nclu1
                ocell  = cells1{clu1(c1)}; % from previous frame
                ccell1 = cells2{clu2(c2)}; % current cell
                
                % Measure how much the orverlap in length
                [olap1] = overlaplens(abs(ocell.center(2)-y01) ,ocell.length,...
                                      abs(ccell1.center(2)-y02),ccell1.length);
                if(olap1>minolap)
                    % they are the same
                    if(prevmaped(clu1(c1))==1)
                        % confirm ID has not been used
                        error(['Cell already mapped! ID:', num2str(clu1(c1))]);
                    end
                    prevmaped(clu1(c1)) = 1;
                    addcells      = [addcells;clu2(c2),clu1(c1),olap1];
                    last2 = c2+1;
                    if(c2<nclu2)
                        ccell2 = cells2{clu2(c2+1)}; % next cell (potential child)
                        [olap2] = overlaplens(abs(ocell.center(2)-y01) ,ocell.length,...
                                              abs(ccell2.center(2)-y02),ccell2.length);
                        if(olap2>minolap)
                            % check if the length sum is
                            % close to parent
                            lsum = ccell1.length+ccell2.length;
                            lpar = ocell.length;
                            if((ccell1.length<ocell.length)&&...
                               (ccell2.length<ocell.length)&&...
                               (abs(lsum-lpar)<5)&&...
                               (abs(lsum-lpar)./lpar<maxcsum))
                                % it is a child                                
                                chicells = [chicells;clu2(c2+1),clu1(c1),olap2];
                                y02   = (ccell1.center(2)+ccell2.center(2))/2;
                                y01   = ocell.center(2);
                                last2 = c2+2;
                            end
                        elseif(ccell1.length<50)
                            % update reference of position, unless cell is too big
                            y02   = ccell1.center(2);
                            y01   = ocell.center(2);
                        end
                    end
                    found = 1;                                                    
                    %next1 = next1+1;                                                    
                    next1 = c1+1;
                    break;
                end
            end
            if(found==0)
                % is not that one
                newcells = [newcells,clu2(c2)];
                last2 = c2+1;
            end
        end
    end
    
end