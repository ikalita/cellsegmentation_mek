function [data] = segfiles2treedata(segfiles,params)

data = struct;
data.xymats   = {};
data.linmats  = [];
data.lenmats  = [];
data.aremats  = [];
data.widmats  = [];
data.cenmat1s = [];
data.cenmat2s = [];
data.parmats  = [];
data.chlmats  = [];
data.gfpmats  = [];
data.mchmats  = [];
data.times    = [];

count = 0;
for xy = 1:length(segfiles)
    segfile  = segfiles{xy};
    temp     = load(segfile);
    frameset = temp.frameset;
    [linmat,parmat,chlmat,xymat] = lineagematrix(frameset,3,1,params);
    [lenmat,aremat,widmat,cenmat1,cenmat2] = getlengthmatrix(linmat,frameset,1,3);
    [gfpmat]               = getfluomatrix(linmat,frameset,1,2,params.rot);
    [mchmat,data.times]    = getfluomatrix(linmat,frameset,1,3,params.rot);
    data.xymats            = {data.xymats{:},xymat{:}};
    data.linmats           = [data.linmats;linmat];
    data.lenmats           = [data.lenmats;lenmat];
    data.aremats           = [data.aremats;aremat];
    data.widmats           = [data.widmats;widmat];
    data.cenmat1s          = [data.cenmat1s;cenmat1];
    data.cenmat2s          = [data.cenmat2s;cenmat2];
    data.gfpmats           = [data.gfpmats;gfpmat];
    data.mchmats           = [data.mchmats;mchmat];
    if(~(size(parmat,2)==size(chlmat,1)))
        error(['Problem with lineage in XY ',num2str(xy), ': ',segfile]);
    end
    data.parmats           = [data.parmats;parmat'+count];
    data.chlmats           = [data.chlmats;chlmat+count];
    count = count+size(linmat,1);             
end

data.touse   = zeros(size(data.lenmats,1),1);
data.nframes = zeros(size(data.lenmats,1),1);
for c = 1:size(data.lenmats)
    pos = find(isnan(data.lenmats(c,:))==0);
    data.nframes(c) = length(pos);
    if(length(pos)>params.minframes)
        data.touse(c)   = 1;
    else
        
    end
end

% $$$ chlmats2 = chlmats;
% $$$ % check for valid divisions
% $$$ for c = 1:size(chlmats)
% $$$     pos = find(isnan(chlmats(c,:))==0);
% $$$     for j = 1:length(pos)
% $$$         if(length(pos(j))==0)
% $$$             chlmats2(c,pos(j)) = NaN;
% $$$         end
% $$$     end
% $$$ end
% $$$ grates = NaN*zeros(size(lenmats));
% $$$ r2s    = NaN*zeros(size(lenmats));
% $$$ for c = 1:size(lenmats)
% $$$     if(touse(c)==1)
% $$$         pos     = find(isnan(lenmats(c,:))==0);        
% $$$         ntracks = length(find(isnan(chlmats(c,:))==0))+1;
% $$$         init = 1;
% $$$         for n = 1:ntracks
% $$$             ds   = find(isnan(chlmats(c,pos(init:end)))==0);
% $$$             if(isempty(ds))
% $$$                 last = length(pos);
% $$$             else
% $$$                 last = ds(1)+(init-2);
% $$$             end
% $$$             if(last<=init)
% $$$                 continue;
% $$$             end
% $$$             lens  = 0.16*lenmats(c,pos(init:last));
% $$$             tvals = times(pos(init:last));
% $$$             tvals = tvals-tvals(1);            
% $$$             for j = 2:length(lens)
% $$$                 if(abs(lens(j)-lens(j-1))./lens(j-1)>0.25)
% $$$                     if(j==length(lens))
% $$$                         lens(j) = lens(j-1);
% $$$                     else
% $$$                         lens(j) = (lens(j+1)+lens(j-1))/2;
% $$$                     end
% $$$                 end
% $$$             end
% $$$             [f0,stats] = fit(tvals',log(lens)','poly1');
% $$$             if(stats.rsquare>0.6)
% $$$                 grates(c,pos(init:last)) = f0.p1;
% $$$                 r2s(c,pos(init:last))    = stats.rsquare;
% $$$             end
% $$$             init = last+1;
% $$$         end
% $$$     end
% $$$ end

end