function [ai] = areaintersect(cell1,cell2)

ai = length(intersect(cell1.PixelIdxList,cell2.PixelIdxList));
ai = ai./length(cell1.PixelIdxList);

end