function [fluomat,time] = getfluomatrix(linmat,frameset,xy,chn,rot)

[~,nxy,~,nits] = size(frameset);

% Retrieve the cell length for each cell in each frame
fluomat = NaN*zeros(size(linmat));
time    = zeros(1,nits);
for it = 1:nits
    temp = load(frameset{1,xy,chn,it}.filepath);
    img  = temp.img;
    img  = rotateimg(img,rot);
    time(it) = temp.deltat/60;
    for c = 1:size(linmat,1)
        if(~isnan(linmat(c,it)))
            cs   = frameset{1,xy,3,it}.cells{linmat(c,it)};
            if(~isfield(cs,'PixelIdxList'))
                pixs = border2pixels(img,cs);
            else
                pixs = cs.PixelIdxList;
            end
            fluomat(c,it) = sum(img(pixs));
        end
    end
end


end