function [fcells,newIDs] = filtercells(treeset,minframes,minparent)

    [nst,nxy] = size(treeset);

    usecells = {};
    newIDs   = {};
    count    = 0;
    for xy = 1:nxy
        tree = treeset{xy};
        usecells{xy} = zeros(length(tree.cells2),1);
        newIDs{xy} = zeros(length(tree.cells2),1);
        for c = 1:length(tree.cells2)
            tree.cells2{c}.nXY = xy;
            if(isempty(tree.cells2{c}.divisions))
                tree.cells2{c}.divisions = 0;
            end
            if((tree.cells2{c}.parent2>minparent)&&...               
                (length(tree.cells2{c}.inframes)>minframes))
                count = count+1;
                usecells{xy}(c) = 1;
                newIDs{xy}(c) = count;
            elseif(tree.cells2{c}.divisions==1)
                
            end
        end
    end

    fcells = {};

    for xy = 1:nxy
        tree = treeset{xy};
        for c = 1:length(tree.cells2)
            if(usecells{xy}(c)==1)
                cell = tree.cells2{c};
                cell.ID2 = newIDs{xy}(c);
                if(cell.parent2>0)
                    cell.parent2 = newIDs{xy}(cell.parent2);
                end
                for n = 1:length(cell.childs)
                    cell.childs(n) = newIDs{xy}(cell.childs(n));
                end
                fcells{newIDs{xy}(c)} = cell;
            end
        end
    end

end