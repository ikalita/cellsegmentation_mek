function [clusters,mpos] = clusterbypos(cells,delta,yorder)

    ncells = length(cells);

    pos = zeros(ncells,2);
    for i = 1:ncells
        pos(i,:) = cells{i}.center;
    end

    ds = abs(pos(:,1)'-pos(:,1));

    clusters = {};
    mpos     = [];
    nclusters  = 0;
    inclusters = zeros(ncells,1);
    for c = 1:ncells
        if(inclusters(c)==0)
            mates = find(ds(c,:)<delta);
            nclusters = nclusters+1;
            [~,spos] = sort(pos(mates,2),yorder);
            clusters{nclusters} = mates(spos);
            mpos(nclusters) = mean(pos(mates,1));
            inclusters(mates) = 1;
        end
    end

end