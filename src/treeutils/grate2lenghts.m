function [plens] = grate2lenghts(grates,times,l0)

plens = zeros(size(times));
plens(1) = l0;
for i = 2:length(times)
    dt = times(i)-times(i-1);
    plens(i) = plens(i-1)*exp(grates(i-1)*dt);
end

end