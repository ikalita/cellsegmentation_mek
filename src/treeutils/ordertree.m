function [index,starts,ends,order,tstarts,tends] = ordertree(cells)

ncells = length(cells);

for c = 1:ncells
    if(isempty(cells{c}.divisions))
        cells{c}.divisions = 0;
    end
end

visited = zeros(ncells,1);
order   = [];
for n = 1:ncells
    if(cells{n}.parent2==0)
        if(cells{n}.divisions==0)
            order  = [order,n];
        else
            childs = cells{n}.childs;
            if(length(childs)==1)
                cells{n}
                error('Cell has only one child');
            end
            order  = [order,childs(1),n,childs(2)];
        end
        visited(n) = 1;
    end
end

while(sum(visited)<ncells)
    for n = 1:ncells        
        if(visited(n)==0)
            pos = find(order==n);
            if(~isempty(pos))
                if(cells{n}.divisions==0)
                    visited(n) = 1;
                else
                    childs = cells{n}.childs;
                    if(length(childs)==1)
                        cells{n}
                        error('Cell has only one child');
                    end
                    order  = [order(1:pos-1),...
                              childs(1),order(pos),childs(2),...
                              order(pos+1:end)];
                    visited(n) = 1;
                end
            end
        end
    end
end

index  = zeros(size(order));
starts = zeros(size(order));
ends   = zeros(size(order));
tstarts = zeros(size(order));
tends   = zeros(size(order));

for n = 1:ncells
    pos = find(order==n);
    index(n)  = pos;
    starts(n) = cells{n}.inframes(1);
    ends(n)   = cells{n}.inframes(end);
    tstarts(n) = cells{n}.times(1);
    tends(n)   = cells{n}.times(end);
end

end