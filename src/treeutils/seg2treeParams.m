function [params] = seg2treeParams(channel,STR1,STR1val,STR2,STR2val,STR3,STR3val,STR4,STR4val,STR5,STR5val,STR6,STR6val,STR7,STR7val,STR8,STR8val,STR9,STR9val)

% Must take the number of the channel used for segmentation
%  
% Valid options
%
% xyp := order preference for matching (X or Y). Image width is Y and
%        image height is Y
%
% xyorder := order preference 'ascend' or 'descend'. Use 'ascend' for
%            cells leaving channels to the bottom of the image
%
% maxdist := max distance of centers for candidates (unless something
%            weird 20 to 50 is fine.
%
% minintc := min relative area intersection [0,1]. 0.1 is fine. Be
%            worried if cells are swiming withing the channel.
%
% quantify := 1 or 0. Does quantification and adds to the structure
%
% fact     := border expansion for bkg, if quantify=1. 1.2 to 2.0 is good.
%
% singlecells := 1 or 0, splits linages into single cells
%

   params = struct;
   OPTIONAL_STRS = {'xyp','xyorder','deltaclus','minolap','quantify','fact','singlecells','verbose','maxcsum'};
   
   params.channel = channel;
   params.('xyp') = 2; % order preference for matching (X or Y)
   params.('xyorder')  = 'ascend'; % order preference
   params.('quantify') = 1; % 1 or 0
   params.('fact')     = 1.8; % border expansion for bkg, if quantify=1
   params.('singlecells') = 1; % 1 or 0, splits linages into single cells
   params.('verbose')     = 1; % 1 or 0, splits linages into single cells
   params.('deltaclus')   = 20;
   params.('minolap')     = 0.2;
   params.('maxcsum')     = 0.2;
   
   if nargin < 1
       error('Channel must be specified');
   elseif (nargin==1)
       
   elseif ~(mod(nargin,2)==1)
       error('Optional inputs must be accompanied by values');
   elseif nargin == 3
       Input_Values = {STR1, STR1val};
   elseif nargin == 5
       Input_Values = {STR1 STR1val; STR2, STR2val};
   elseif nargin == 7
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val};
   elseif nargin == 9
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val};
   elseif nargin == 11
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val; STR5, STR5val};
   elseif nargin == 13
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val; STR5, STR5val; STR6, STR6val};
   elseif nargin == 15
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val; STR5, STR5val; STR6, STR6val; STR7, STR7val};
   elseif nargin == 17
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val; STR5, STR5val; STR6, STR6val; STR7, STR7val; STR8, STR8val};
   elseif nargin == 19
       Input_Values = {STR1 STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val; STR5, STR5val; STR6, STR6val; STR7, STR7val; STR8, STR8val; STR9, STR9val};
   elseif nargin > 19
       error('At most 19 inputs are allowed');
   end
   
   for i=1:((nargin - 1)/2)
        STR = Input_Values{i,1};
        STRval = Input_Values{i,2};
        z = strmatch(STR,OPTIONAL_STRS,'exact');
        if z == 1
            params.('xyp') = STRval; 
        elseif z == 2
            params.('xyorder')  = STRval; 
        elseif z == 3
            params.('deltaclus')  = STRval; 
        elseif z == 4
            params.('minolap')  = STRval; 
        elseif z == 5
            params.('quantify') = STRval; 
        elseif z == 6
            params.('fact')     = STRval; 
        elseif z == 7
            params.('singlecells') = STRval; 
        elseif z == 8
            params.('verbose')  = STRval; 
        elseif z == 9
            params.('maxcsum')  = STRval; 
        else
            error (['Invalid option: ',STR]);
        end
    end

end 