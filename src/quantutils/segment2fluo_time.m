function [areas,fluos,bkgs,tfluos,sfluos,lens,fluos_time] = segment2fluo_time(frameset,segchn,fact,replstr)

%fact = 0.2;

nframes = length(frameset);

[nsteps,nxys,nchns,nits] = size(frameset);    % [z-stacks?, pics nb, channels, timepoints]

areas  = zeros(40000,1);
lens  = zeros(40000,1);
fluos  = zeros(40000,3);
bkgs   = zeros(40000,3);
tfluos = zeros(40000,3);
sfluos = zeros(40000,3);

fluos_time = zeros(40000,3);
fluos_time_sort = zeros(40000,3);

tcells = 0;
for st = 1:nsteps
    for xy = 1:nxys
        for it = 1:nits
            %if frameset{st,xy,segchn,it}.cells
            cells = frameset{st,xy,segchn,it}.cells;
            ncells = length(cells);
            borders = cell(1,ncells);
            for c = 1:ncells
                borders{c} = expandborder(cells{c}.center,cells{c}.border,fact);
                areas(tcells+c) = cells{c}.area;
                lens(tcells+c) = cells{c}.length;
            end
            for ch = 1:nchns
                imgpath = frameset{st,xy,ch,it}.filepath;
                imgpath = regexprep(imgpath,'C:\\microscopeIM\\',replstr);
                for foo = 1:10
                    imgpath = regexprep(imgpath,'\','/');
                end
                if(strcmp(imgpath(end-3:end),'.mat'))
                    temp = load(imgpath);
                    img = double(temp.img);
                else % TIF file
                    img = loadTIF(imgpath);
                end
                for c = 1:ncells
                    fluos(tcells+c,ch)  = mean(img(cells{c}.PixelIdxList));
                    bkgs(tcells+c,ch)   = mean(readborder(img,borders{c}));
                    tfluos(tcells+c,ch) = sum(img(cells{c}.PixelIdxList));
                    sfluos(tcells+c,ch) = std(img(cells{c}.PixelIdxList));
                end
            end
            fluos_time(tcells+1:tcells+ncells,ch) = fluos(tcells+1:tcells+ncells,ch);
            fluos_time(tcells+1:tcells+ncells,3) = it;
            tcells = tcells+ncells;
        end
    end
end    

areas  = areas(1:tcells);
lens   = lens(1:tcells);
fluos  = fluos(1:tcells,:);
bkgs   = bkgs(1:tcells,:);
tfluos = tfluos(1:tcells,:);
sfluos = sfluos(1:tcells,:);

ccell = 1;
for it = 1:nits
    for i = 1:tcells
    if fluos_time(i,3) == it
        fluos_time_sort(ccell,:) = fluos_time(i,:);
        ccell = ccell + 1;
    end
    end
end

fluos_time = fluos_time_sort(1:tcells,:);

end