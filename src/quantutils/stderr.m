function [err] = stderr(vals)

nvals = length(vals);

err = (std(vals)./mean(vals))./sqrt(nvals);

end