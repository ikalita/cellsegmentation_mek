function [areas,fluos,bkgs,tfluos,sfluos,lens, ob_pro_tot] = segment2fluo2(frameset,segchn,fact,replstr)

%fact = 0.2;

nframes = length(frameset);

[nsteps,nxys,nchns,nits] = size(frameset);    % [z-stacks?, pics nb, channels, timepoints]

areas  = zeros(40000,1);
lens  = zeros(40000,1);
fluos  = zeros(40000,3);
bkgs   = zeros(40000,3);
tfluos = zeros(40000,3);
sfluos = zeros(40000,3);


tcells = 0;
for st = 1:nsteps
    for xy = 1:nxys
        ob_pro=struct;
        for it = 1:nits
            cells = frameset{st,xy,segchn,it}.cells;
            ncells = length(cells);
            borders = cell(1,ncells);
            for c = 1:ncells
                borders{c} = expandborder(cells{c}.center,cells{c}.border,fact);
                areas(tcells+c) = cells{c}.area;
                lens(tcells+c) = cells{c}.length;
                
%               if frameset{st,xy,segchn,it}.cells  
                    ob_pro(c).Area = cells{c}.area;
                    ob_pro(c).PixelIdxList = cells{c}.PixelIdxList;
                    ob_pro(c).Centroid = cells{c}.center;
                    ob_pro(c).MajorAxisLength = cells{c}.length;
                
            end
            for ch = 1:nchns
                imgpath = frameset{st,xy,ch,it}.filepath;
                imgpath = regexprep(imgpath,'C:\\microscopeIM\\',replstr);
                for foo = 1:10
                    imgpath = regexprep(imgpath,'\','/');
                end
                if(strcmp(imgpath(end-3:end),'.mat'))
                    temp = load(imgpath);
                    img = double(temp.img);
                else % TIF file
                    img = loadTIF(imgpath);
                end
                for c = 1:ncells
                    fluos(tcells+c,ch)  = mean(img(cells{c}.PixelIdxList));
                    bkgs(tcells+c,ch)   = mean(readborder(img,borders{c}));
                    tfluos(tcells+c,ch) = sum(img(cells{c}.PixelIdxList));
                    sfluos(tcells+c,ch) = std(img(cells{c}.PixelIdxList));
                end
                if ch == 3
                    for c = 1:ncells
                      ob_pro(c).fluos = mean(img(cells{c}.PixelIdxList));  
                    end
                end
                    
                end
            end
            tcells = tcells+ncells;
            ob_pro_tot{xy} = ob_pro;
            clear ob_pro;
        end
    
    end
   

areas  = areas(1:tcells);
lens   = lens(1:tcells);
fluos  = fluos(1:tcells,:);
bkgs   = bkgs(1:tcells,:);
tfluos = tfluos(1:tcells,:);
sfluos = sfluos(1:tcells,:);




end