function [border2] = expandborder2(center,border,fact)

    [nq,~] = size(border);
    border2 = zeros(size(border));
    for q = 1:nq
        bl = border(q,:);
        dc = bl-[center(2),center(1)];
        [a,r] = cart2pol(dc(1),dc(2));
        %border2(q,:) = bl+fact.*[cos(a),sin(a)];%round(fact.*dc);
        border2(q,:) = bl+fact.*[cos(a),sin(a)].*sqrt(r);%round(fact.*dc);
    end

end