# CellSegmentation_MEK

This MATLAB-based pipeline was developed by Sebastian Jaramillo-Riveri to identify positions of bacterial _Escherichia coli_ cells from microscopy bright-field defocused images. 

The algorithm of cell segmentation is based on cell-edge detection by passing an input image through a low-pass filter. Very similar to the the method described in [S. Jaramillo-Riveri's 2019 thesis](https://era.ed.ac.uk/handle/1842/36092), with minor variations on the construction of the filters. Segmentation output can be manually corrected in the accompanied graphical interface (GUI). 

The project contains the main file (segmentation_main.m), graphical interface application (segmentGUI.m) and a repository (src) with utilized functions as well as with additional plotting and visualizing scripts.

