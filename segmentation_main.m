clear all

foldername = pwd; % a folder with bright-field images
prefix     = ''; % names of bright-field images
channels   = {'w10 Brightfield'}; % channel name
indexes    = [1:2]; % indexes of bright-field images to segment

%% Program files

addpath(genpath([foldername '\src\'])); 

%% Create a file which will be read by segmentGUI

makeTIFset2(foldername,...
            prefix,...
            channels,...
            indexes,...
            [],...
            foldername);

%% Segmentation and manual correction with segmentGUI

segmentGUI;





